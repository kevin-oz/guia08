/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.GenericMethods;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.TipoEstadoReservaFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoEstadoReserva;

/**
 *
 * @author kevin Figueroa
 */
@Named(value = "backing_bean")
@ViewScoped
public class TipoEstadoReservaBean extends GenericBean<TipoEstadoReserva> implements Serializable {

    public TipoEstadoReservaBean() {
    }

    @EJB
    TipoEstadoReservaFacadeLocal ter_facade;
    TipoEstadoReserva ter = new TipoEstadoReserva();

    public TipoEstadoReservaFacadeLocal getTer_facade() {
        return ter_facade;
    }

    public void setTer_facade(TipoEstadoReservaFacadeLocal ter_facade) {
        this.ter_facade = ter_facade;
    }

    public TipoEstadoReserva getTer() {
        return ter;
    }

    public void setTer(TipoEstadoReserva ter) {
        this.ter = ter;
    }

    public List<TipoEstadoReserva> getLista() {
        return lista;
    }

    public void setLista(List<TipoEstadoReserva> lista) {
        this.lista = lista;
    }

    @PostConstruct
    public void init() {
        llenar();
    }

    @Override
    protected GenericMethods<TipoEstadoReserva> getFacadeLocal() {
        return ter_facade;
    }

    @Override
    public TipoEstadoReserva getEntity() {
        return ter;
    }

    @Override
    public void crear() {
        super.crear();
       this.ter= new TipoEstadoReserva();
    }  
       
}
