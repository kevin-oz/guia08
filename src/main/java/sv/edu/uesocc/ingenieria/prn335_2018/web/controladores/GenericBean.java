/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.GenericMethods;

/**
 *
 * @author kevin Figueroa
 * @param <T>
 */
public abstract class GenericBean<T> implements Serializable {

    List<T> lista;

    /**
     * todos los registros existentes
     */
    public void llenar() {
        if (getFacadeLocal().findAll() != null) {
            this.lista = getFacadeLocal().findAll();
        } else {
            this.lista = Collections.EMPTY_LIST;
        }
    }

    /**
     * mensaje para mostrar informacion de procesos
     * @param summary  mensaje a mostar dependiendo donde se implemente
     */
    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    
   
   /**
    * metodo generico para persistir un registro 
    */ 
    public void crear() {
        if (getFacadeLocal() != null) {
            try {
                getFacadeLocal().create(getEntity());
                llenar();
                addMessage("Registro creado correctamente.");
            } catch (Exception ex) {
                System.out.println("Error: " + ex);
                addMessage("Error al crear registro.");
            }
        }
    }

    protected abstract GenericMethods<T> getFacadeLocal();

    public abstract T getEntity();

}
